﻿namespace StringBuilder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridViewData = new System.Windows.Forms.DataGridView();
            this.buttonAddColumn = new System.Windows.Forms.Button();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.buttonAddSep = new System.Windows.Forms.Button();
            this.textBoxSep = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonClearInput = new System.Windows.Forms.Button();
            this.richTextBoxOutput = new System.Windows.Forms.RichTextBox();
            this.textBoxColumnA = new System.Windows.Forms.TextBox();
            this.textBoxColumnB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSwapCol = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewData
            // 
            this.dataGridViewData.AllowUserToAddRows = false;
            this.dataGridViewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewData.Location = new System.Drawing.Point(490, 44);
            this.dataGridViewData.Name = "dataGridViewData";
            this.dataGridViewData.Size = new System.Drawing.Size(437, 374);
            this.dataGridViewData.TabIndex = 0;
            this.dataGridViewData.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewData_CellContentDoubleClick);
            this.dataGridViewData.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewData_ColumnHeaderMouseClick);
            // 
            // buttonAddColumn
            // 
            this.buttonAddColumn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonAddColumn.Location = new System.Drawing.Point(324, 44);
            this.buttonAddColumn.Name = "buttonAddColumn";
            this.buttonAddColumn.Size = new System.Drawing.Size(134, 39);
            this.buttonAddColumn.TabIndex = 2;
            this.buttonAddColumn.Text = "Dodaj kolumnę";
            this.buttonAddColumn.UseVisualStyleBackColor = false;
            this.buttonAddColumn.Click += new System.EventHandler(this.buttonAddColumn_Click);
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonGenerate.Location = new System.Drawing.Point(324, 257);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(134, 76);
            this.buttonGenerate.TabIndex = 3;
            this.buttonGenerate.Text = "GENERUJ";
            this.buttonGenerate.UseVisualStyleBackColor = false;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // buttonAddSep
            // 
            this.buttonAddSep.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonAddSep.Location = new System.Drawing.Point(324, 125);
            this.buttonAddSep.Name = "buttonAddSep";
            this.buttonAddSep.Size = new System.Drawing.Size(134, 35);
            this.buttonAddSep.TabIndex = 4;
            this.buttonAddSep.Text = "Dodaj separator";
            this.buttonAddSep.UseVisualStyleBackColor = false;
            this.buttonAddSep.Click += new System.EventHandler(this.buttonAddSep_Click);
            // 
            // textBoxSep
            // 
            this.textBoxSep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSep.Location = new System.Drawing.Point(324, 166);
            this.textBoxSep.Name = "textBoxSep";
            this.textBoxSep.Size = new System.Drawing.Size(134, 20);
            this.textBoxSep.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StringBuilder.Properties.Resources.bob_the_builder;
            this.pictureBox1.Location = new System.Drawing.Point(12, 452);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(160, 217);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // textBoxInput
            // 
            this.textBoxInput.Location = new System.Drawing.Point(12, 46);
            this.textBoxInput.Multiline = true;
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxInput.Size = new System.Drawing.Size(284, 374);
            this.textBoxInput.TabIndex = 7;
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonClear.Location = new System.Drawing.Point(324, 374);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(134, 44);
            this.buttonClear.TabIndex = 8;
            this.buttonClear.Text = "Czyść wszystko";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonClearInput
            // 
            this.buttonClearInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonClearInput.Location = new System.Drawing.Point(221, 17);
            this.buttonClearInput.Name = "buttonClearInput";
            this.buttonClearInput.Size = new System.Drawing.Size(75, 23);
            this.buttonClearInput.TabIndex = 9;
            this.buttonClearInput.Text = "czyść";
            this.buttonClearInput.UseVisualStyleBackColor = false;
            this.buttonClearInput.Click += new System.EventHandler(this.buttonClearInput_Click);
            // 
            // richTextBoxOutput
            // 
            this.richTextBoxOutput.Location = new System.Drawing.Point(236, 452);
            this.richTextBoxOutput.Name = "richTextBoxOutput";
            this.richTextBoxOutput.Size = new System.Drawing.Size(691, 203);
            this.richTextBoxOutput.TabIndex = 10;
            this.richTextBoxOutput.Text = "";
            // 
            // textBoxColumnA
            // 
            this.textBoxColumnA.Location = new System.Drawing.Point(582, 20);
            this.textBoxColumnA.Name = "textBoxColumnA";
            this.textBoxColumnA.Size = new System.Drawing.Size(57, 20);
            this.textBoxColumnA.TabIndex = 11;
            // 
            // textBoxColumnB
            // 
            this.textBoxColumnB.Location = new System.Drawing.Point(765, 20);
            this.textBoxColumnB.Name = "textBoxColumnB";
            this.textBoxColumnB.Size = new System.Drawing.Size(57, 20);
            this.textBoxColumnB.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(581, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Kolumna A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(764, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Kolumna B";
            // 
            // buttonSwapCol
            // 
            this.buttonSwapCol.BackgroundImage = global::StringBuilder.Properties.Resources.exchange;
            this.buttonSwapCol.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSwapCol.Location = new System.Drawing.Point(682, 6);
            this.buttonSwapCol.Name = "buttonSwapCol";
            this.buttonSwapCol.Size = new System.Drawing.Size(43, 34);
            this.buttonSwapCol.TabIndex = 15;
            this.buttonSwapCol.UseVisualStyleBackColor = true;
            this.buttonSwapCol.Click += new System.EventHandler(this.buttonSwapCol_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(939, 681);
            this.Controls.Add(this.buttonSwapCol);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxColumnB);
            this.Controls.Add(this.textBoxColumnA);
            this.Controls.Add(this.richTextBoxOutput);
            this.Controls.Add(this.buttonClearInput);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.textBoxInput);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxSep);
            this.Controls.Add(this.buttonAddSep);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.buttonAddColumn);
            this.Controls.Add(this.dataGridViewData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "-- String The Builder --";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewData;
        private System.Windows.Forms.Button buttonAddColumn;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Button buttonAddSep;
        private System.Windows.Forms.TextBox textBoxSep;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonClearInput;
        private System.Windows.Forms.RichTextBox richTextBoxOutput;
        private System.Windows.Forms.TextBox textBoxColumnA;
        private System.Windows.Forms.TextBox textBoxColumnB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSwapCol;
    }
}

