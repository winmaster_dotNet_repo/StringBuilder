﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StringBuilder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            richTextBoxOutput.Text = "";
            string currentLine = "";
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            for (int i = 0; i <dataGridViewData.Rows.Count; i++)
            {
                currentLine = "";
                for(int j= 0; j<dataGridViewData.Columns.Count; j++)
                {
                    currentLine += dataGridViewData.Rows[i].Cells[j].Value;
                }
                sb.AppendLine(currentLine);
            }
            richTextBoxOutput.Text = sb.ToString().Trim(); 
        }

        private void buttonAddColumn_Click(object sender, EventArgs e)
        {
            List<string> context = textBoxInput.Text.Split('\n').ToList();
            int columnCounter = dataGridViewData.Columns.Count;
            int rowCounter = dataGridViewData.Rows.Count;

            dataGridViewData.Columns.Add(columnCounter.ToString(), "Kolumna" + columnCounter.ToString());

            if (columnCounter == 0)
            {
                for (int i = 0; i < context.Count(); i++)
                    dataGridViewData.Rows.Add();
            }
            else
            {
                if(context.Count > rowCounter)
                {
                    int missingRows = context.Count - rowCounter;
                    for(int i = 0; i< missingRows; i++)
                    {
                        dataGridViewData.Rows.Add();
                    }
                }
            }

            for ( int i = 0; i < context.Count(); i++)
            {
                string element = context[i].TrimEnd();
                dataGridViewData.Rows[i].Cells[columnCounter].Value = element;
            }

        }

        private void buttonAddSep_Click(object sender, EventArgs e)
        {
            int columnCounter = dataGridViewData.Columns.Count;
            dataGridViewData.Columns.Add(columnCounter.ToString(), "Kolumna" + columnCounter.ToString());

            int rowCounter = dataGridViewData.Rows.Count;

            if (columnCounter == 0 || rowCounter == 0)
            {
                    dataGridViewData.Rows.Add();
                    rowCounter++;
            }

            for (int i = 0; i <rowCounter; i++)
            {
                dataGridViewData.Rows[i].Cells[columnCounter].Value = textBoxSep.Text;
            }

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxInput.Text = "";
            richTextBoxOutput.Text = "";
            textBoxColumnA.Text = "";
            textBoxColumnB.Text = "";
            dataGridViewData.Rows.Clear();
            dataGridViewData.Columns.Clear();
            dataGridViewData.Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\t\tTak damy radę! \n\n Program StringBuilder made  by winmaster  in 2017", "Czy damy radę ?");
        }

        private void buttonClearInput_Click(object sender, EventArgs e)
        {
            textBoxInput.Text = "";
        }

        private void dataGridViewData_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int columnNumber = e.ColumnIndex;
            int rowNumber = e.RowIndex;
            try
            {
                var cellValue = dataGridViewData.Rows[rowNumber].Cells[columnNumber].Value;
                int rowCounter = dataGridViewData.Rows.Count;

                for (int i = rowNumber; i < rowCounter; i++)
                {
                    dataGridViewData.Rows[i].Cells[columnNumber].Value = cellValue;
                }
            }
           catch
            {

            }
        }

        private void dataGridViewData_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usunąć kolumnę?", "Usuwanie kolumny", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int columnNumber = e.ColumnIndex;
                dataGridViewData.Columns.Remove(dataGridViewData.Columns[columnNumber]);
            }
        }

        private void buttonSwapCol_Click(object sender, EventArgs e)
        {
            int firstColumnIndex;
            int secondColumnIndex;

            try
            {
                firstColumnIndex = int.Parse(textBoxColumnA.Text);
                secondColumnIndex = int.Parse(textBoxColumnB.Text);

                if (firstColumnIndex != secondColumnIndex && firstColumnIndex <= dataGridViewData.Columns.Count && secondColumnIndex <= dataGridViewData.Columns.Count)
                {
                    for (int i = 0; i < dataGridViewData.Rows.Count; i++)
                    {
                        var temp = dataGridViewData.Rows[i].Cells[firstColumnIndex].Value;
                        dataGridViewData.Rows[i].Cells[firstColumnIndex].Value = dataGridViewData.Rows[i].Cells[secondColumnIndex].Value;
                        dataGridViewData.Rows[i].Cells[secondColumnIndex].Value = temp;
                    }
                    dataGridViewData.Refresh();
                }
                else
                {
                    textBoxColumnA.Text = "";
                    textBoxColumnB.Text = "";
                }
            }
            catch
            {
                textBoxColumnA.Text = "";
                textBoxColumnB.Text = "";
            }
        }
    }
}
